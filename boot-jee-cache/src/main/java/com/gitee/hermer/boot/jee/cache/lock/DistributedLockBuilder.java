package com.gitee.hermer.boot.jee.cache.lock;

import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import com.gitee.hermer.boot.jee.cache.BootCache;
import com.gitee.hermer.boot.jee.commons.exception.ErrorCode;
import com.gitee.hermer.boot.jee.commons.log.Logger;
import com.gitee.hermer.boot.jee.commons.number.IntegerUtils;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;
import com.gitee.hermer.boot.jee.commons.verify.Assert;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

/**
 * 
 * @ClassName: DistributedLockBuilder
 * @Description: NX分布式锁实现
 * @author:  涂孟超
 * @date: 2017年10月21日 下午11:33:28
 */
public class DistributedLockBuilder {

	private static Logger logger = Logger.getLogger(DistributedLockBuilder.class);


	private static Set<HostAndPort> nodes = new HashSet<HostAndPort>();
	
	private static JedisCluster cluster = null;
	static{
		try {
			Properties config = BootCache.loadConfig();
			if(StringUtils.defaultIfEmpty(config.getProperty("redis.policy"), "single").equals("cluster")){
				String[] hosts = StringUtils.split(config.getProperty("redis.host"), ",");
				for (String host : hosts) {
					String[] arr = StringUtils.split(host, ":");
					nodes.add(new HostAndPort(arr[0],IntegerUtils.defaultIfError(arr[1], 0)));
				}
				cluster = new JedisCluster(nodes);
			}
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
	}


	/**
	 * 构造一个默认的分布式锁，默认3秒超时
	 * @param lockName
	 * @return
	 */
	public static DistributedLock builder(String lockName){
		Assert.notEmptyCode(nodes,ErrorCode.SYSTEM_ERROR,"boot-cache.properties The redis cluster is not enabled");
		RedisDistributedLockImpl lock = new RedisDistributedLockImpl(lockName);
		lock.setRedis(cluster);
		return lock;
	}

	/**
	 * 构造一个分布式锁，以传入的timeout为超时时间
	 * @param lockName
	 * @param timeOut
	 * @return
	 */
	public static DistributedLock builder(String lockName, long timeOut){
		Assert.notEmptyCode(nodes,ErrorCode.SYSTEM_ERROR,"boot-cache.properties not find redis.cluster.nodes");
		RedisDistributedLockImpl lock = new RedisDistributedLockImpl(lockName, timeOut);
		lock.setRedis(cluster);
		return lock;
	}


}


package com.gitee.hermer.boot.jee.cache.ehcache;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gitee.hermer.boot.jee.cache.CacheException;
import com.gitee.hermer.boot.jee.cache.CacheExpiredListener;
import com.gitee.hermer.boot.jee.cache.CacheProvider;

import net.sf.ehcache.CacheManager;


public class EhCacheProvider implements CacheProvider {

	private final static Logger log = LoggerFactory.getLogger(EhCacheProvider.class);
	public static String KEY_EHCACHE_NAME = "ehcache.name";
	public static String KEY_EHCACHE_CONFIG_XML = "ehcache.configXml";

	private CacheManager manager;
	private ConcurrentHashMap<String, EhCache> _CacheManager ;

	@Override
	public String name() {
		return "ehcache";
	}

    
    @Override
	public EhCache buildCache(String name, boolean autoCreate, CacheExpiredListener listener) throws CacheException {
    	EhCache ehcache = _CacheManager.get(name);
    	if(ehcache == null && autoCreate){
		    try {
	            synchronized(_CacheManager){
	            	ehcache = _CacheManager.get(name);
	            	if(ehcache == null){
			            net.sf.ehcache.Cache cache = manager.getCache(name);
			            if (cache == null) {
			                log.warn("Could not find configuration [" + name + "]; using defaults.");
			                manager.addCache(name);
			                cache = manager.getCache(name);
			                log.debug("started EHCache region: " + name);                
			            }
			            ehcache = new EhCache(cache, listener);
			            _CacheManager.put(name, ehcache);
	            	}
	            }
		    }
	        catch (net.sf.ehcache.CacheException e) {
	            throw new CacheException(e);
	        }
    	}
        return ehcache;
    }

	
	@Override
	public void start(Properties props) throws CacheException {
		if (manager != null) {
            log.warn("Attempt to restart an already started EhCacheProvider.");
            return;
        }
		
		
		String ehcacheName = (String)props.get(KEY_EHCACHE_NAME);
		if (ehcacheName != null && ehcacheName.trim().length() > 0)
			manager = CacheManager.getCacheManager(ehcacheName);
		if (manager == null) {
			
			if (props.containsKey(KEY_EHCACHE_CONFIG_XML)) {
				manager = new CacheManager(props.getProperty(KEY_EHCACHE_CONFIG_XML));
			} else {
				
				manager = CacheManager.getInstance();
			}
		}
        _CacheManager = new ConcurrentHashMap<String, EhCache>();
	}

	
	@Override
	public void stop() {
		if (manager != null) {
            manager.shutdown();
            _CacheManager.clear();
            manager = null;
        }
	}

}

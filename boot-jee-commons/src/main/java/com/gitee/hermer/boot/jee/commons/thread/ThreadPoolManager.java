
package com.gitee.hermer.boot.jee.commons.thread;

import java.util.concurrent.Future;

public class ThreadPoolManager {

	private static final FixedThreadPool pool = FixedThreadPool.getInstance();

	public static Future<?> submit(Runnable task) {
		return pool.get().submit(task);
	}
	
	public static <T> Future<T>  submit(Runnable task,T t) {
		return pool.get().submit(task,t);
	}
}

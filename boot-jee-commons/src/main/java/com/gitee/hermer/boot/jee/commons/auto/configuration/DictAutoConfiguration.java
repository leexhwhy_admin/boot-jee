package com.gitee.hermer.boot.jee.commons.auto.configuration;

import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.gitee.hermer.boot.jee.commons.dict.BootProperties;
import com.gitee.hermer.boot.jee.commons.dict.domain.DictProperties;
import com.gitee.hermer.boot.jee.commons.dict.domain.SimpleDictProperties;

@Configuration                                                                                                                              
@ConditionalOnClass(BootProperties.class)  
@AutoConfigureOrder(1)
public class DictAutoConfiguration {

	@Bean
	@ConditionalOnMissingBean(SimpleDictProperties.class)    
	public SimpleDictProperties getSimpleDictProperties(){
		return new SimpleDictProperties();
	}


	@Bean                                                                                                                                  
	@ConditionalOnMissingBean(BootProperties.class)                                                                                           
	public BootProperties getBootProperties(DictProperties[] properties) {                                                                                                    
		return new BootProperties(properties);                                                                                     
	}  

}

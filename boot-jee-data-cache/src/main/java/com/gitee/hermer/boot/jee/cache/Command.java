package com.gitee.hermer.boot.jee.cache;

public class Command {
	
	public enum Operation{
		onDelete,
		onClear
	}
	
	private Operation operation;
	private String region;
	private String key;
	
	
	public Operation getOperation() {
		return operation;
	}
	public String getRegion() {
		return region;
	}
	public String getKey() {
		return key;
	}
	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	
	
	
	

}

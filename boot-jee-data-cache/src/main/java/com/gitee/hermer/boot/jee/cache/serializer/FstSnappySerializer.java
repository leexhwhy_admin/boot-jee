package com.gitee.hermer.boot.jee.cache.serializer;

import java.io.Serializable;

import net.sf.ehcache.CacheException;

import org.springframework.data.redis.serializer.SerializationException;
import org.xerial.snappy.Snappy;


public class FstSnappySerializer implements Serializer {

	private final Serializer inner;

	public FstSnappySerializer() {
		this(new FSTSerializer());
	}

	public FstSnappySerializer(Serializer innerSerializer) {
		this.inner = innerSerializer;
	}
	
	
	@Override
	public String name() {
		return "fst_snappy";
	}
	
	@Override
	public byte[] serialize(Serializable t) throws SerializationException{
		try {
			return Snappy.compress(inner.serialize(t));
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}

	@Override
	public Serializable deserialize(byte[] bytes) throws SerializationException{
		if (bytes == null || bytes.length == 0)
			return null;
		try {
			return inner.deserialize(Snappy.uncompress(bytes));
		} catch (Exception e) {
			throw new CacheException(e);
		}
	}
}


package com.gitee.hermer.boot.jee.cache.ehcache;

import java.io.File;
import java.util.concurrent.ConcurrentHashMap;

import com.gitee.hermer.boot.jee.cache.CacheListener;
import com.gitee.hermer.boot.jee.cache.CacheProvider;
import com.gitee.hermer.boot.jee.cache.exception.CacheException;
import com.gitee.hermer.boot.jee.cache.properties.BootCacheProperties;
import com.gitee.hermer.boot.jee.commons.collection.StringCache;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;

import net.sf.ehcache.CacheManager;


public class EhCacheProvider extends UtilsContext implements CacheProvider {


	private CacheManager manager;
	private ConcurrentHashMap<String, EhCacheClient> cacheManager ;

	@Override
	public String name() {
		return "ehcache";
	}


	@Override
	public EhCacheClient buildCache(String name, boolean autoCreate, CacheListener listener) throws CacheException {
		EhCacheClient ehcache = cacheManager.get(name);
		if(ehcache == null && autoCreate){
			try {
				synchronized(cacheManager){
					ehcache = cacheManager.get(name);
					if(ehcache == null){
						net.sf.ehcache.Cache cache = manager.getCache(name);
						if (cache == null) {
							warn("Could not find configuration [" + name + "]; using defaults.");
							manager.addCache(name);
							cache = manager.getCache(name);
							debug("started EHCache region: " + name);                
						}
						ehcache = new EhCacheClient(cache, listener);
						cacheManager.put(name, ehcache);
					}
				}
			}
			catch (net.sf.ehcache.CacheException e) {
				throw new CacheException(e);
			}
		}
		return ehcache;
	}

	@Override
	public void start(BootCacheProperties properties) throws CacheException {
		if (manager != null) {
			warn("Attempt to restart an already started EhCacheProvider.");
			return;
		}
		if(properties instanceof BootCacheProperties){
			BootCacheProperties props = properties;
			if (StringUtils.isNotBlank(props.getEhcacheName()))
				manager = CacheManager.getCacheManager(props.getEhcacheName());
			if (manager == null) {

				if (StringUtils.isNotBlank(props.getEhcacheConfigXml())) {
					manager = new CacheManager(new StringCache(System.getProperty("java.class.path").split(";")[0])
							.append(File.separator).append(StringUtils.trim(props.getEhcacheConfigXml())).toString());
				} else {

					manager = CacheManager.getInstance();
				}
			}
			cacheManager = new ConcurrentHashMap<String, EhCacheClient>();

		}
	}


	@Override
	public void stop() {
		if (manager != null) {
			manager.shutdown();
			cacheManager.clear();
			manager = null;
		}
	}

}

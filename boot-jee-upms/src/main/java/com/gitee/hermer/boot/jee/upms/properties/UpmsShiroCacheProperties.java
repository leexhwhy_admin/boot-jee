package com.gitee.hermer.boot.jee.upms.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.gitee.hermer.boot.jee.commons.number.IntegerUtils;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;

@ConfigurationProperties(prefix = "com.boot.jee.upms.shiro.cache")  
public class UpmsShiroCacheProperties {
	
	
	private String prefix;
	
	private Integer sessionTimeOut;

	public String getPrefix() {
		return StringUtils.defaultIfEmpty(prefix, "boot-cache-shiro");
	}

	public Integer getSessionTimeOut() {
		return IntegerUtils.defaultIfSmallerThan0(sessionTimeOut, 30*60);
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	@ConfigurationProperties(value="session-timeout")
	public void setSessionTimeOut(Integer sessionTimeOut) {
		this.sessionTimeOut = sessionTimeOut;
	}
	
	
}

package com.gitee.hermer.boot.jee.generate.template;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import com.gitee.hermer.boot.jee.commons.utils.FileUtils;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;
import com.gitee.hermer.boot.jee.commons.verify.Assert;

public class BjeTemplateUtils {
	
	public static String template(String bjeFileName,Map<String, String> params){
		
		Assert.hasText(bjeFileName);
		Assert.notEmpty(params);
		InputStream is = FileUtils.getResourceStream(bjeFileName);
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));   
		StringBuilder sb = new StringBuilder();   
		String line = null;   
		try {   
			while ((line = reader.readLine()) != null) {   
				sb.append(line + "\n");   
			}   
		} catch (IOException e) {   
			e.printStackTrace();   
		} finally {   
			try {   
				is.close();   
			} catch (IOException e) {   
				e.printStackTrace();   
			}   
		}
		String text = sb.toString();
		Assert.hasText(text);
		for (String key : params.keySet()) {
			text = StringUtils.replaceAll(text, params.get(key),"#\\{"+key+"\\}");
		}
		text = StringUtils.replaceAll(text,"" , "#\\{.*\\}");
		return text;
		
	}

}

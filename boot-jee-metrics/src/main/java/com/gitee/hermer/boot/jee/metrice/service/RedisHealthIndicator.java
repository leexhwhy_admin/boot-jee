package com.gitee.hermer.boot.jee.metrice.service;

import java.util.Map;

import org.springframework.boot.actuate.health.HealthIndicator;

import com.gitee.hermer.boot.jee.commons.collection.CollectionUtils;
import com.gitee.hermer.boot.jee.metrice.indicator.BootJeeHealthIndicator;

public class RedisHealthIndicator extends BootJeeHealthIndicator implements HealthIndicator{

	@Override
	public Map<String, String> metrice() {
		// TODO Auto-generated method stub
        Map<String, String> params = CollectionUtils.newHashMap();
//        params.put("state", IntegerUtils.defaultIfError(RedisCacheService.ping(), 0).toString());
		return params;
	}
}
